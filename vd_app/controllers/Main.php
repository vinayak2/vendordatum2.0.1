<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	/* Class Admin
	 * Owned by: Cuion Technologies Pvt. Ltd.
	 * Owner URL: http://www.cuion.in/
	 * Author(s): Lijo George, 
	 */

    public function __construct()
    {
        parent::__construct();
		if (!$this->ion_auth->logged_in()){
			redirect(base_url('/login'), 'refresh');
		}elseif(!$this->ion_auth->is_admin()){
			$this->data['actor'] = $this->ion_auth->user()->row();
			$this->data['actor']->role = $this->ion_auth->get_users_groups()->row();
		}
    }

	public function index($page = null)
	{
        $this->load->model(array('Post_model', 'Category_model'));
        if(null !== $this->input->post('remove')){
            $this->post = new Post_model();
            $this->post->load($this->input->post('post_id'));
            if(!$this->post->post_id){
                show_404();
            }
            $this->post->delete();
            $this->session->set_flashdata('error_message', 'Item has been removed successfully!');
            redirect(base_url(), 'refresh');
        }else{
            $this->load->helper('form');
            // Form Validation.
            $this->load->library('form_validation');
            $this->form_validation->set_rules(array(
                array(
                    'field' => 'title',
                    'label' => 'title',
                    'rules' => 'required',
                ),
            )); 
            $this->form_validation->set_error_delimiters('"', '",');
            
            if (!$this->form_validation->run()) {

                $this->data['error_message'] = validation_errors();

                //$this->data['categories'] = $this->Category_model->get();

    	        $this->data['announcements'] = $this->Post_model->getPosts('announcements', $this->data['actor']->id);
    	        if(($used_space = sizeof($this->data['announcements'])) < 9){
    	        	for($i=$used_space; $i<9; $i++){
    	        		array_push($this->data['announcements'], new Post_model());
    	        	}
    	        }
    	        $this->data['accomplishments'] = $this->Post_model->getPosts('accomplishments', $this->data['actor']->id);
    	        if(($used_space = sizeof($this->data['accomplishments'])) < 9){
    	        	for($i=$used_space; $i<9; $i++){
    	        		array_push($this->data['accomplishments'], new Post_model());
    	        	}
    	        }

                // Pagination 

                $this->load->library('pagination');
                $config = array(
                    'prefix' => 'main/',
                    'per_page' => 6,
                    'total_rows' => $this->Post_model->totalrows('announcements'),
                );
                $this->pagination->initialize($config);
                $this->data['latest_posts']=$this->Post_model->pagedata('announcements', $config['per_page'], $page);

                $this->data['pagination']=$this->pagination->create_links();

    			$this->render('dashboard', $this->data, array('user_panel'));
            }else {
    	        $this->post = new Post_model();
                if(null !== $this->input->post('post_id')){
                    $this->post->load($this->input->post('post_id'));
                }
                $this->post->post_type = $this->input->post('post_type');
                $this->post->title = $this->input->post('title');
                $this->post->description = $this->input->post('description');
                $this->post->category = $this->input->post('category');

                // File Upload Library
                $this->load->library('upload');
                $this->error_message = '';

                // post attachment configuration 
                $config = array(
                    'upload_path' => 'assets/uploads/post/attachment',
                    'allowed_types' => '*',
                    'max_size' => 2048,
                );
                $this->upload->initialize($config);
                
                // File Upload Validation.
                if (isset($_FILES['post_attachment']['error']) && ($_FILES['post_attachment']['error'] != 4) && !$this->upload->do_upload('post_attachment')){
                    $this->error_message .= 'Error uploading attachment! Max:2MB,';
                }else{
                    $upload_data = $this->upload->data();
                    if($upload_data['file_name']) {
                        $this->post->attachment = $upload_data['file_name'];
                    }
                }

                // Post image configuration
                $config = array(
                    'upload_path' => 'assets/uploads/post',
                    'allowed_types' => 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG',
                    'min_width' => 580,
                    'min_height' => 580,
                );
                $this->upload->initialize($config);
                
                // File Upload Validation.
                if (isset($_FILES['post_image']['error']) && ($_FILES['post_image']['error'] != 4) && !$this->upload->do_upload('post_image')){
                    $this->error_message .= 'Error uploading image! Min:580px, Max:2MB, JPG|PNG|GIF,';
                }else{
                    $upload_data = $this->upload->data();
                    if($upload_data['file_name']) {
                        // Optimize uploaded image
                        $terms = array(
                            'source' => 'assets/uploads/post/'.$upload_data['file_name'],
                            'result' => 'assets/uploads/post/resized/'.$upload_data['file_name'],
                            'source_width' => $upload_data['image_width'],
                            'source_height' => $upload_data['image_height'],
                            'result_width' => 580,
                            'result_height' => 580,
                        );
                        $this->BestCropImage($terms);
                        $this->post->image = $upload_data['file_name'];
                    }
                }

                $this->post->publish = $this->input->post('publish');
                $this->post->added_by = $this->input->post('added_by');
                if(null !== $this->input->post('publish')){
                	$this->post->publish = 1;
    	            $this->load->helper('date');
    	            $this->post->published_on = now();
                }else{
                	$this->post->publish = 0;
    	            $this->post->published_on = NULL;
                }
                $this->post->added_by = $this->ion_auth->get_user_id();

                $this->post->save();

                if(!empty($this->error_message)){
                    $this->session->set_flashdata('error_message', $this->error_message);
                }

                redirect(base_url(), 'refresh');
            }
        }
	}

    
    public function profile($id = null)
    {
        $this->profile = $this->ion_auth->user($id)->row();

        if(!empty($this->profile)){
            $this->load->library('table');
            $this->table->set_template(array('table_open' => '<table id="profileTable" class="table table-bordered table-striped">'));
            $this->table->set_heading('<strong>Name</strong>', $this->profile->first_name.' '.$this->profile->last_name);
            $this->table->add_row('<strong>Designation</strong>', $this->profile->designation);
            $this->table->add_row('<strong>Company</strong>', $this->profile->company);
            $this->table->add_row('<strong>Industry</strong>', $this->profile->industry);
            $this->table->add_row('<strong>Business</strong>', $this->profile->business);
            $this->table->add_row('<strong>Address</strong>', $this->profile->address);
            $this->table->add_row('<strong>Location</strong>', $this->profile->location);
            $this->table->add_row('<strong>Phone</strong>', $this->profile->phone);
            $this->table->add_row('<strong>Mobile</strong>', $this->profile->mobile);
            $this->data['profile_username'] = $this->profile->email;
            $this->data['profile_member_id'] = $this->profile->member_id;
            $this->data['profile_table'] = $this->table->generate();
            $this->data['profile_image'] = $this->utilities->prep_display_image($this->profile->image, 'profile');
            $this->data['account_settings'] = ($this->profile->id === $this->data['actor']->id) ? true: false;
            $this->load->model('Post_model');
            $this->data['announcements'] = $this->Post_model->getPosts('announcements', $this->profile->id);
            $this->data['accomplishments'] = $this->Post_model->getPosts('accomplishments', $this->profile->id);
        }
        $this->render('profile_page', $this->data, array('user_panel'));
    }

    public function post($data = 'page', $offset = null)
    {
        $this->load->model('Post_model');
        if($data == 'page'){
            $this->data['request']['search'] = $this->input->get('search');
            $this->data['request']['title'] = $this->input->get('title');
            $this->data['request']['company'] = $this->input->get('company');
            $this->data['request']['location'] = $this->input->get('location');

            // Pagination 

            $this->load->library('pagination');
            $config = array(
                'prefix' => 'post/page/',
                'reuse_query_string' => true,
                'per_page' => 6,
                'total_rows' => $this->Post_model->totalrows1('announcements', $this->data['request']),
            );
            $this->pagination->initialize($config);
            $this->data['posts']=$this->Post_model->pagedata1('announcements',$config['per_page'], $offset, $this->data['request']);

            $this->data['pagination']=$this->pagination->create_links();

            $this->render('search_page', $this->data, array('user_panel'));
        }else{
            $this->data['post'] = new Post_model();
            $this->data['post']->load($data);
            if(!$this->data['post']->post_id){
                show_404();
            }else{
                $this->data['post_image'] = $this->utilities->prep_display_image($this->data['post']->image, 'post');
                $this->profile = $this->ion_auth->user($this->data['post']->added_by)->row();
                $this->data['profile_username'] = $this->profile->email;
                $this->data['profile_member_id'] = $this->profile->member_id;
                $this->data['profile_image'] = $this->utilities->prep_display_image($this->profile->image, 'profile');
                $this->render('product_page', $this->data, array('user_panel'));
            }
        }
    }

    public function profile_edit()
    {
        $this->render('account_details', $this->data, array('user_panel'));
    }
    
    public function publisher()
    {
        $this->render('manage_publishers', $this->data, array('user_panel'));
    }
    
    public function knowledge_bank()
    {
        $this->render('knowledge_bank', $this->data, array('user_panel'));
    }

    public function question_asked()
    {
        $this->render('question_asked_viewpage', $this->data, array('user_panel'));
    }

    public function answer_questions()
    {
        $this->render('answer_questions', $this->data, array('user_panel'));
    }

    public function search_page()
    {
        $this->render('search_page', $this->data, array('user_panel'));
    }

    public function user1($id)
    {
        return $this->data['actor'] = $this->ion_auth->user($id)->row();
    }    
    
    public function ajax_autoload_category()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $keyword = $this->input->post('keyword');
        if($keyword){
            $this->load->model('Category_model');
            $this->data['category_list'] = $this->Category_model->getCategoryByKeyword($keyword);
            $this->load->view('ajax/auto_category', $this->data);
        }
    }

    public function test_email()
    {
        $this->load->library('email');

        // To use SMTP
        $config['protocol'] = 'smtp';

        // If you're using Amazon SES in a region other than US West (Oregon), 
        // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
        // endpoint in the appropriate region.
        $config['smtp_host'] = 'email-smtp.us-east-1.amazonaws.com';

        // Replace smtp_username with your Amazon SES SMTP user name.
        $config['smtp_user'] = 'AKIAI237S6MOL532WW5Q';

        // Replace smtp_password with your Amazon SES SMTP password.
        $config['smtp_pass'] = 'AoyWdv+bshWBm7mO6tfLgj9FqOstMpPn3ChLq4YBhVb2';

        // Enable TLS encryption over port 587
        $config['smtp_port'] = 587;
        $config['smtp_crypto'] = 'tls';

        // To send HTML-formatted email
        $config['mailtype'] = 'html';
        $config['validate'] = TRUE;

        $this->email->initialize($config);

        // Replace sender@example.com with your "From" address. 
        // This address must be verified with Amazon SES.
        $this->email->from('vendordatum.cuion@gmail.com', 'Vendordatum');

        // Replace recipient@example.com with a "To" address. 
        //  If your account is still in the sandbox, this address must be verified.
        $this->email->to('lijoagain@gmail.com');

        // The subject line of the email
        $this->email->subject('Amazon SES test (SMTP interface accessed using PHP)');

        // The HTML-formatted body of the email
        $this->email->message('<h1>Email Test</h1>
            <p>This email was sent through the 
            <a href="https://aws.amazon.com/ses">Amazon SES</a> SMTP
            interface using the <a href="https://www.codeigniter.com/">
            Codeigniter Email Class</a> class.</p>');

        // The alternative email body; this is only displayed when a recipient opens the email in a non-HTML email client.
        // The \r\n represents a line break.
        $this->email->set_alt_message("Email Test\r\nThis email was sent through the Amazon SES SMTP interface using the Codeigniter Email Class.");

        if(!$this->email->send()) {
            echo "Email not sent. " , PHP_EOL;
            show_error($this->email->print_debugger());
        } else {
            echo "Email sent!" , PHP_EOL;
        }
    }
}