<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="row mt-3">
      <div class="col-md-12">
        <div class="border px-2">
          <div class="card border-0 rounded-0 mt-2">
            <div class="card-header bg-vd rounded-0 p-2">
              <h5 class="text-center text-light m-0">Profile Details</h5>
            </div>
			<?php echo form_open_multipart(); ?>
            <div class="row row-eq-height card-body">
              <div class="col-md-4">
                  <div class="form-group">
                    <label for="first_name"><?php echo $first_name['title']; ?></label>
                    <?php echo form_input($first_name);?>
                  </div>
                  <div class="form-group">
                    <label for="last_name"><?php echo $last_name['title']; ?></label>
                    <?php echo form_input($last_name);?>
                  </div>
                  <div class="form-group">
                    <label for="email"><?php echo $email['title']; ?></label>
                    <?php echo form_input($email);?>
                  </div>
                  <div class="form-group">
                    <label for="designation"><?php echo $designation['title']; ?></label>
                    <?php echo form_input($designation);?>
                  </div>
                  <div class="form-group">
                    <label for="phone"><?php echo $phone['title']; ?></label>
                    <?php echo form_input($phone);?>
                  </div>
                  <div class="form-group">
                    <label for="mobile"><?php echo $mobile['title']; ?></label>
                    <?php echo form_input($mobile);?>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label for="company"><?php echo $company['title']; ?></label>
                    <?php echo form_input($company);?>
                  </div>
                  <div class="form-group">
                    <label for="location"><?php echo $location['title']; ?></label>
                    <?php echo form_input($location);?>
                  </div>
                  <div class="form-group">
                    <label for="industry"><?php echo $industry['title']; ?></label>
                    <?php echo form_input($industry);?>
                  </div>
                  <div class="form-group">
                    <label for="business"><?php echo $business['extra']['title']; ?></label>
                    <?php echo form_dropdown('business', $business['options'], $business['selected'], $business['extra']);?>
                  </div>
                  <div class="form-group">
                    <label for="address"><?php echo $address['title']; ?></label>
                    <?php echo form_textarea($address);?>
                  </div>

                  <?php if ($this->ion_auth->is_admin()): ?>

                      <h3><?php echo lang('edit_user_groups_heading');?></h3>
                      <?php foreach ($groups as $group):?>
                          <label class="checkbox">
                          <?php
                              $gID=$group['id'];
                              $checked = null;
                              $item = null;
                              foreach($currentGroups as $grp) {
                                  if ($gID == $grp->id) {
                                      $checked= ' checked="checked"';
                                  break;
                                  }
                              }
                          ?>
                          <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                          <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                          </label>
                      <?php endforeach?>

                  <?php endif ?>

                  <?php echo form_hidden('id', $user->id);?>
                  <?php // echo form_hidden($csrf); ?>

                
              </div>
              <div class="col-md-4">
                <label for="image"><?php echo $image['title']; ?></label>
                <?php echo $profile_image; ?>
                  <div class="form-group">
                    <div class="custom-file">
                      <?php echo form_upload($image);?>
                      <label class="custom-file-label rounded-0" for="post_image">Choose Image</label>
                    </div>
                  </div>
                  <div class="form-group clearfix">
                    <?php echo form_submit('submit', 'Update', array('class' => 'btn bg-vd text-light float-right')); ?>
                  </div>
              </div>
            </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>