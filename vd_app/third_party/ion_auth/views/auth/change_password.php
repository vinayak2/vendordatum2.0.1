<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="row justify-content-center mt-5">
    <div class="col-4">
      <div class="card border-0 rounded-0">
        <div class="card-header bg-white border border-bottom-0 border-info rounded-0">
          <h5 class="text-center vd">Change Password</h5>
        </div>
        <div class="card-body border border-info">
          <?php echo form_open("change_password");?>
            <div class="form-group">
              <?php echo form_input($old_password);?>
            </div>
            <div class="form-group">
              <label for="new" class="text-muted">At least 8 characters long.</label>
              <?php echo form_input($new_password);?>
            </div>
            <div class="form-group">
              <?php echo form_input($new_password_confirm);?>
            </div>
            <?php echo form_input($user_id);?>
            <div>
              <?php echo form_submit('submit', 'CHANGE', array('class' => 'btn btn-outline-info btn-block col-6 mx-auto'));?>
            </div>
          <?php echo form_close();?>
        </div>
      </div>
    </div>
  </div>