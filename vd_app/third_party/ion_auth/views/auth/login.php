<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="row justify-content-around mt-5">
    <div class="col-sm-10 col-md-6 col-lg-4 p-3">
      <div class="card border-0 rounded-0">
        <div class="card-header bg-white border border-bottom-0 border-info rounded-0">
          <h5 class="text-center vd">Vendor Profile</h5>
        </div>
        <div class="card-body border border-info">
          <?php echo form_open("auth/login", '', array('role' => '2'));?>
            <div class="form-group">
              <?php echo form_input($identity);?>
            </div>
            <div class="form-group">
              <?php echo form_input($password);?>
            </div>
            <div>
              <?php echo form_submit('submit', 'LOG IN', array('class' => 'btn btn-outline-info'));?>
              <a href="remind_password" class="btn btn-outline-info float-right">REMIND PASSWORD</a>
            </div>
          <?php echo form_close();?>
          <br/>
          <h5 class="text-center">NEW USER?</h5>
          <p class="text-center">CREATE A NEW VENDOR PROFILE</p>
          <a href="onboard/vendor" class="btn btn-info btn-block bg-vd col-6 mx-auto" role="button">ON BOARD</a>
        </div>
      </div>
    </div>
    <div class="col-sm-10 col-md-6 col-lg-4 p-3">
      <div class="card border-0 rounded-0">
        <div class="card-header bg-white border border-bottom-0 border-info rounded-0">
          <h5 class="text-center vd">Buyer Profile</h5>
        </div>
        <div class="card-body border border-info">
          <?php echo form_open("auth/login", '', array('role' => '3'));?>
            <div class="form-group">
              <?php echo form_input($identity);?>
            </div>
            <div class="form-group">
              <?php echo form_input($password);?>
            </div>
            <div>
              <?php echo form_submit('submit', 'LOG IN', array('class' => 'btn btn-outline-info'));?>
              <a href="remind_password" class="btn btn-outline-info float-right">REMIND PASSWORD</a>
            </div>
          <?php echo form_close();?>
          <br/>
          <h5 class="text-center">NEW USER?</h5>
          <p class="text-center">CREATE A NEW BUYER PROFILE</p>
          <a href="onboard/buyer" class="btn btn-info btn-block bg-vd col-6 mx-auto" role="button">ON BOARD</a>
        </div>
      </div>
    </div>
  </div>