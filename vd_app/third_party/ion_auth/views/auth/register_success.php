<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="row justify-content-center mt-5">
    <div class="col-4">
      <div class="card border-0 rounded-0">
        <div class="card-header bg-white border border-bottom-0 border-info rounded-0">
          <h5 class="text-center vd">Confirm Your Email Address</h5>
        </div>
        <div class="card-body border border-info">
            <p class="text-center text-muted">A confirmation email has been sent to your registered email ID. Click on the confirmation link in the email to activate your account.</p>
        </div>
      </div>
    </div>
  </div>