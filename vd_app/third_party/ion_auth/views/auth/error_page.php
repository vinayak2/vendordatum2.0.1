<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h1 class="text-center" style="line-height: 100px;"> 404! </h1>
<h4 class="text-center">This page does not exist!</h4>
<p class="text-center">Please click <a href="<?php echo base_url(); ?>">here</a> to go to the dashboard.</p>