<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row justify-content-center my-5">
	<div class="col-8 border">
		<div class="card border-0 rounded-0 mt-2">
		  <div class="card-header bg-vd rounded-0">
		  	<h5 class="text-center text-light"><?php echo $title; ?></h5>
		  </div>
		  <div class="card-body">
		      <?php echo form_open_multipart($action, '', $hidden);?>
			    <div class="row">
				    <div class="col-md-6">
			            <div class="form-group">
			              <?php echo form_input($first_name);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($last_name);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($email);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($designation);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($phone);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($mobile);?>
			            </div>
				    </div>
				    <div class="col-md-6">
			            <div class="form-group">
			              <?php echo form_input($company);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($location);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($industry);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_dropdown('business', $business['options'], $business['selected'], $business['extra']);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($password);?>
			            </div>
			            <div class="form-group">
			              <?php echo form_input($password_confirm);?>
			            </div>
				    </div>
				   </div>
				   <div class="row justify-content-end">
			        <div class="col-md-6">
			            <div class="form-group">
							<div class="custom-file">
							  <input type="file" class="custom-file-input" id="post_image" name="image">
							  <label class="custom-file-label rounded-0" for="post_image">Choose Image</label>
							</div>
						</div>
			            <div class="form-group">
			            	<?php echo form_submit('submit', 'Onboard', array('class' => 'btn bg-vd text-light')); ?>
			            </div>
			        </div>
			    </div>
		      <?php echo form_close();?>
		  </div>
		</div>
	</div>
</div>

<div class="row justify-content-center my-5">
	<div class="col-8">
		  <ul class="list-inline text-center">
		    <?php if(isset($potential_customers) & !empty($potential_customers)): ?>
			    <?php foreach($potential_customers as $value):?>
			    	<li class="list-inline-item small text-secondary m-3"><?php echo $value; ?></li>
				<?php endforeach; ?>
			<?php endif; ?>
		  </ul>
	</div>
</div>