<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="row justify-content-center mt-5">
    <div class="col-4">
      <div class="card border-0 rounded-0">
        <div class="card-header bg-white border border-bottom-0 border-info rounded-0">
          <h5 class="text-center vd">Remind Password</h5>
        </div>
        <div class="card-body border border-info">
          <?php echo form_open("remind_password");?>
            <div class="form-group">
              <?php echo form_input($identity);?>
            </div>
            <p class="text-center text-muted">Please enter your email address. You will receive a link to reset password via email.</p>
            <div>
              <?php echo form_submit('submit', 'REMIND', array('class' => 'btn btn-outline-info btn-block col-6 mx-auto'));?>
            </div>
          <?php echo form_close();?>
        </div>
      </div>
    </div>
  </div>
