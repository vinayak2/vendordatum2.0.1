<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* This Model extends the MY_Model and represent
 * DATABASE: 'vendordatum_ci'
 * TABLE: 'category'
 * Owned by: Cuion Technologies Pvt. Ltd.
 * Owner URL: http://www.cuion.in/
 * Author(s): Lijo George, 
 */

class Category_model extends MY_Model {
	
	/* Set database table name */
	const DB_TABLE = 'category';
	
	/* Set primary key for this table */
	const DB_TABLE_PK = 'category_id';

	/* Category table unique key. Primary key. */
	public $category_id;

	/* Display ID, Unique. */
	public $display_id;

	/* Category title. */
	public $title;

	/* Timestamp. Record first added. */
	public $added_on;
	
	public function getCategoryByKeyword($keyword = '')
	{
		if($keyword != ''){
			$this->db->select('title');
			$this->db->like('title', $keyword, 'after');
			$this->db->order_by('title', 'ASC');
			return $this->db->get('category', 10)->result_object();
		}
	}
}