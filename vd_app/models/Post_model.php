<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* This Model extends the MY_Model and represent
 * DATABASE: 'vendordatum_ci'
 * TABLE: 'post'
 * Owned by: Cuion Technologies Pvt. Ltd.
 * Owner URL: http://www.cuion.in/
 * Author(s): Lijo George, 
 */

class Post_model extends MY_Model {
	
	/* Set database table name */
	const DB_TABLE = 'post';
	
	/* Set primary key for this table */
	const DB_TABLE_PK = 'post_id';

	/* Product table unique key. Primary key. */
	public $post_id;

	/* Type of post. */
	public $post_type;

	/* Product title. */
	public $title;

	/* Description. */
	public $description;

	/* Post category. */
	public $category;

	/* Description. */
	public $image;

	/* Description. */
	public $attachment;

	/* Publish post. */
	public $publish;
	
	/* User ID */
	public $added_by;

	/* Timestamp. Record first added. */
	public $added_on;

	/* Timestamp. Record published. */
	public $published_on;

	// public $CI = NULL;

	/*
	*	@param $post_type varchar
	*	@param $actor_id integer
	*	@param $limit integer
	*/

	 // public function __construct() {
  //       parent::__construct();
  //       $this->CI = & get_instance();
  //   }

	public function getPosts($post_type = NULL, $actor_id = NULL, $req = array())
	{
		$conditions = array();
		if($post_type){
			$conditions['post_type'] = $post_type;
		}
		if($actor_id){
			$conditions['added_by'] = $actor_id;
			$this->db->order_by('added_on', 'ASC');
			$limit = NULL;
			if($actor_id !== $this->ion_auth->user()->row()->id){
				$conditions['publish'] = 1;
			}
		}else{
			$this->db->order_by('added_on', 'DESC');
			$limit = 6;
		}
		if(isset($req['search']) && !empty($req['search'])){
			$this->db->like('post.title', $req['search'], 'both');
		}
		if(isset($req['title']) && !empty($req['title'])){
			$conditions['title'] = $req['title'];
		}
		// echo $this->db->last_query();
		return $this->get($limit, NULL, $conditions);

	}
	
	public function getAuthorName()
	{
		$user = $this->ion_auth->user($this->added_by)->row();
		return $user->first_name.' '.$user->last_name;
	}

	public function getCompanyName()
	{
		return $this->ion_auth->user($this->added_by)->row()->company;
	}

	public function showImage()
	{
		return $this->utilities->prep_display_image($this->image, 'post');
	}
 

	public function totalrows($post_type = NULL)
	{   
		$conditions = array();
		if($post_type){
			$conditions['post_type'] = $post_type;
		} 
        $this->db->where($conditions);
		return $this->getCount();
	}

	public function totalrows1($post_type = NULL, $req = array())
	{   
		$conditions = array();
		if($post_type){
			$conditions['post_type'] = $post_type;
		}		
        $this->db->where($conditions);                
		if(isset($req['search']) && !empty($req['search'])){
			$this->db->like('post.title', $req['search'], 'both');
		}
		return $this->getCount();
	} 


	 
	public function pagedata($post_type = NULL,$perpage,$offset)
	{   
		$conditions = array();
		if($post_type){
			$conditions['post_type'] = $post_type;
		}
		
		if(!$offset){$offset=0;}
		//$this->db->where($conditions);
		return $this->get($perpage,$offset,$conditions);
		// echo $this->db->last_query();
        // return $query->result();
	}


	public function pagedata1($post_type = NULL,$perpage,$offset,$req = array())
	{   
		// echo $perpage;echo "<br>";
	 //    echo $offset;echo "<br>";

		$conditions = array();
		if($post_type){
			$conditions['post_type'] = $post_type;
		}
		if(isset($req['search']) && !empty($req['search'])){
			$this->db->join('actors', 'post.added_by = actors.id', 'left');
			$this->db->like('post.title', $req['search'], 'both');
			$this->db->or_like('actors.first_name', $req['search'], 'both');
			$this->db->or_like('actors.last_name', $req['search'], 'both');
			$this->db->or_like('actors.company', $req['search'], 'both');
			$this->db->or_like('actors.location', $req['search'], 'both');
		}
		
		if(!$offset){$offset=0;}
				//$this->db->where($conditions);
		
		 return $this->get($perpage,$offset,$conditions); 
		     // echo $this->db->last_query();
             // return $dataa;
		// echo $this->db->last_query();
        //return $query->result();
	}





}