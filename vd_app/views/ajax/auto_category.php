<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!empty($category_list)) { ?>
		<ul class="category_list">
	<?php foreach($category_list as $value) { ?>
			<li class="category-item"><?php echo $value->title; ?></li>
	<?php } ?>
		</ul>
<?php }