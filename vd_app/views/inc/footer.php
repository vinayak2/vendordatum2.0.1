<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
        </div>


        <footer class="footer fixed-bottom bg-vd">
          <div class="container">
            <span class="small text-light"><a class="text-light" href="<?php echo base_url(); ?>">Vendordatum</a> &copy; <?php echo date('Y'); ?></span>
          </div>
        </footer>

        <!-- vendordatum v2.0.1 scripts-->

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        
        <!-- Fancybox -->
        <script src="<?php echo base_url();?>assets/plugins/fancybox-master/jquery.fancybox.min.js"></script>

        <!-- Owl Carousel -->
        <script src="<?php echo base_url();?>assets/plugins/owl-carousel/owl.carousel.min.js"></script>
		
        <!-- iao-alert -->
        <script src="<?php echo base_url('assets/js/iao-alert.jquery.min.js'); ?>"></script>

        <!-- select2 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <!-- initiating iao-alert -->
        <script>
            <?php if(isset($error_message) && !empty($error_message)){ ?>
                $.each([<?php echo $error_message; ?>], function( index, value ) {
                    $.iaoAlert({ msg: value});
                });
            <?php } ?>
            <?php if($message = $this->session->flashdata('error_message')){ ?>
                $.iaoAlert({ msg: "<?php echo $message; ?>"});
            <?php } ?>
        </script>

        <!-- initiating Fancybox -->
        <script>
            $("[data-fancybox]").fancybox({
                buttons: ["close"]
            });
        </script>

        <!-- initiating Owl Carousel -->
        <script>
			$(document).ready(function(){
			  $('.owl-carousel').owlCarousel({
				loop:true,
				margin:10,
				responsiveClass:true,
				responsive:{
					0:{
						items:1,
						nav:true
					},
					600:{
						items:3,
						nav:false
					},
					1000:{
						items:5,
						nav:true,
						loop:false
					}
				}
			})
			});
        </script>

        <!-- bootstrap custom file input -->
        <script type="application/javascript">
            $('input[type="file"]').change(function(e){
                $(this).next('.custom-file-label').html(e.target.files[0].name);
            });
        </script>
               
        <!-- Custom Scripts -->
        <script> var variable = { base_url: "<?php echo base_url(); ?>" }; </script>
        <script src="<?php echo base_url('assets/js/script_v2.js'); ?>"></script>

    </body>
</html>