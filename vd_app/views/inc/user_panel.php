<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
            <!--head left Section Start-->
            <div class="row justify-content-end">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="media border p-3 business-card">
                        <div class="media-body">
                            <h5><?php echo $actor->first_name. ' ' .$actor->last_name; ?></h5>
                            <span><?php echo $actor->designation; ?></br><?php echo $actor->company.', '.$actor->location; ?></span>
                        </div>
                        <div class="dropdown">
                            <?php if($actor->image): ?>
                                <img src="<?php echo base_url('assets/uploads/profile/resized/').$actor->image; ?>" class="ml-3 mt-3 rounded-circle img-thumbnail dropdown-toggle profile" alt="Profile" data-toggle="dropdown">
                            <?php else: ?>
                                <div class="dropdown-toggle profile_placeholder" data-toggle="dropdown"><span><?php echo $actor->role->description[0]; ?></span></div>
                            <?php endif; ?>                            
                            <div class="dropdown-menu">
                              <a class="dropdown-item" href="<?php echo base_url('profile'); ?>">View Profile</a>
                              <a class="dropdown-item" href="<?php echo base_url('logout'); ?>">Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--head left Section End-->