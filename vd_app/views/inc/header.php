<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
    <head>
        <title>Vendordatum</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- vendordatum v2.0.1 styles -->

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

        <!-- Calibri Font -->
        <link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css" integrity="sha384-wnAC7ln+XN0UKdcPvJvtqIH3jOjs9pnKnq9qX68ImXvOGz2JuFoEiCjT8jyZQX2z" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css" integrity="sha384-HbmWTHay9psM8qyzEKPc8odH4DsOuzdejtnr+OFtDmOcIVnhgReQ4GZBH7uwcjf6" crossorigin="anonymous">
        
        <!-- Fancybox -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fancybox-master/jquery.fancybox.min.css">
        
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/owl-carousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/owl-carousel/assets/owl.theme.default.min.css">

        <!-- iao-alert -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/iao-alert.min.css'); ?>">

        <!-- select2 -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

        <!-- Custom Styles -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style_v2.css'); ?>">


    </head>
    <body>

        <nav class="navbar navbar-expand navbar-dark bg-vd">
            <div class="container justify-content-sm-end">
                <a class="navbar-brand" href="<?php echo base_url(); ?>">Vendordatum</a>
            </div>
        </nav>

        <div class="container">