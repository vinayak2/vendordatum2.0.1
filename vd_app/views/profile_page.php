<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	 
         <div class="row mt-3">
			<div class="col-md-12">
				<div class="border px-2">
					<div class="card border-0 rounded-0 mt-2">
						<div class="card-header bg-vd rounded-0 p-2">
							<h5 class="text-center text-light m-0">Profile Details</h5>
						</div>
						<div class="row row-eq-height card-body">
							<div class="col-md-8">
						    <?php echo $profile_table; ?>
							</div>
							<div class="col-md-4">
								<h5 class="text-center"><?php echo $profile_username; ?></h5>
								<?php echo $profile_image; ?>
								<?php if($account_settings): ?>
								<div class="text-center"><a href="<?php echo base_url('settings'); ?>" class="btn btn-link" role="button">Account Settings</a></div>
								<div class="text-center"><a href="<?php echo base_url('change_password'); ?>" class="btn btn-link" role="button">Change Password</a></div>
								<?php endif;?>
							</div>
						</div>
					<?php if(isset($announcements) && !empty($announcements)): ?>	
						<div class="card-header bg-vd rounded-0 p-2">
							<h5 class="text-center text-light m-0">Catalog - Products / Services</h5>
						</div>
						<div class="row row-eq-height card-body owl-carousel owl-theme">						
						<?php foreach($announcements as $item): ?>
							<div>
								<a href="<?php echo base_url('post/').$item->post_id; ?>"><h5 class="text-center"><?php echo $item->title; ?></h5></a>
								<?php echo $item->showImage(); ?>
							</div>
						<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<?php if(isset($accomplishments) && !empty($accomplishments)): ?>	
						<div class="card-header bg-vd rounded-0 p-2">
							<h5 class="text-center text-light m-0">Accomplishments - Projects delivered</h5>
						</div>
						<div class="row row-eq-height card-body owl-carousel owl-theme">						
						<?php foreach($accomplishments as $item): ?>
							<div>
								<a href="<?php echo base_url('post/').$item->post_id; ?>"><h5 class="text-center"><?php echo $item->title; ?></h5></a>
								<?php echo $item->showImage(); ?>
							</div>
						<?php endforeach; ?>
						</div>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>