<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
				 
         <div class="row mt-3">
				 <div class="col-md-12">
				  	<div class="border px-2">
						<div class="card border-0 rounded-0 mt-2">   
<style type="text/css">
input.radio:empty {
	margin-left: -999px;
}
input.radio:empty ~ label {
	position: relative;
	float: left;
	line-height: 2.5em;
	text-indent: 3.25em;
	margin-top: 2em;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

input.radio:empty ~ label:before {
	position: absolute;
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	content: '';
	width: 2.5em;
	background: #D1D3D4;
	border-radius: 3px 0 0 3px;
}

input.radio:hover:not(:checked) ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #C2C2C2;
}

input.radio:hover:not(:checked) ~ label {
	color: #888;
}

input.radio:checked ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #9CE2AE;
	background-color: #4DCB6D;
}

input.radio:checked ~ label {
	color: #777;
}

input.radio:focus ~ label:before {
	box-shadow: 0 0 0 3px #999;
}

.form-control {
margin-top: 10px;
}

table span{
	    background: green;
    border-radius: 5px 5px 5px 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px;
    color: #fff;
    font-size: 22px;
}

}
</style>
<div class="row">
<div class="col-md-6">
							 <div class="card-header  bg-vd rounded-0 p-2">
						  	<h5 class="text-center text-light m-0">Profile Details</h5>
						  </div>
						  <div class="row row-eq-height card-body">
						  		<input class="form-control" type="text" name="buyer" id="buyer" placeholder="Buyer">
						  		<br><br>
						  		<input class="form-control" type="text" name="name" id="name" placeholder="Name">
						  		<br><br>
						  		<input class="form-control" type="text" name="email" id="email" placeholder="Email">
						  		<br><br>
						  <input class="form-control" type="text" name="company" id="company" placeholder="Company">
						  <br><br>
					<input class="form-control" type="text" name="mobileno" id="mobileno" placeholder="Mobile No">
<br> 
                      <button class="form-control btn btn-primary"  type="button" style="width:20%;text-align:center;background:#4bacc6;border-color:#4bacc6;">Update</button>

						  	</div>
						  	</div>


						 <div class="col-md-6">

						  	<div class="card-header   bg-vd rounded-0 p-2" >
						  	<h5 class="text-center text-light m-0">Profile Photo</h5>
						  	 
						  </div>
						 
						  <div class="row row-eq-height card-body" style="display:inline;">
						  	 <center>
						  		  <!-- <div class="col-md-12" > -->
							   	<img  src="<?php echo base_url('assets/images/placeholder.png'); ?>" class="img-thumbnail img-fluid rounded-0" alt="Product" style="width:50%;height:50%;display:block;"> <br>
							   <button class="btn btn-primary" type="button" style="background:#4bacc6;border-color:#4bacc6;">Select Files</button><br>
							   <span>Maximum Upload File Size 500 KB</span><br>
							   <button class="btn btn-primary" type="button" style="background:#4bacc6;border-color:#4bacc6;">Update</button>
</center>
							  <!-- </div> -->

						  	</div>
						  	</div>



		                  <div class="col-md-6">
						  	 <div class="card-header   bg-vd rounded-0 p-2" >
						  	<h5 class="text-center text-light m-0">Change Password</h5>
						  	 
						  </div>
						 
						  <div class="row row-eq-height card-body">
						  <input type="text" class="form-control" id="currentpassword" name="currentpassword"><br>
						  <input type="text" class="form-control" id="newpassword" name="newpassword"><br>
						  <input type="text" class="form-control" id="confirmpassword" name="confirmpassword"><br>
						  <br>
						  <button class="btn btn-primary" type="button" style="margin-top: 10px;background:#4bacc6;border-color:#4bacc6;">Change Now</button><br>

							  <!-- </div> -->

						  	</div>
						  	</div>
 
		 
</div></div>
					</div>
				  </div>
				 </div>
				 

			 