<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
				 
         <div class="row mt-3">
				 <div class="col-md-12">
				  	<div class="border px-2">
						<div class="card border-0 rounded-0 mt-2">   
<style type="text/css">
input.radio:empty {
	margin-left: -999px;
}
input.radio:empty ~ label {
	position: relative;
	float: left;
	line-height: 2.5em;
	text-indent: 3.25em;
	margin-top: 2em;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

input.radio:empty ~ label:before {
	position: absolute;
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	content: '';
	width: 2.5em;
	background: #D1D3D4;
	border-radius: 3px 0 0 3px;
}

input.radio:hover:not(:checked) ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #C2C2C2;
}

input.radio:hover:not(:checked) ~ label {
	color: #888;
}

input.radio:checked ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #9CE2AE;
	background-color: #4DCB6D;
}

input.radio:checked ~ label {
	color: #777;
}

input.radio:focus ~ label:before {
	box-shadow: 0 0 0 3px #999;
}

.form-control {
margin-top: 10px;
}

table span{
	    background: green;
    border-radius: 5px 5px 5px 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px;
    color: #fff;
    font-size: 22px;
}
.btn-primary{
	margin-left:5px;
}

}
</style>
<div class="row">
<div class="col-md-6">
							 <div class="card-header  bg-vd rounded-0 p-2">
						  	<h5 class="text-center text-light m-0">Ask a Question</h5>
						  </div>
						  <div class="row row-eq-height card-body">
	<textarea class="form-control"  name="question" id="question" placeholder="Type in your question"></textarea>
						  		 
                      <button class="form-control btn btn-primary"  type="button" style="width:20%;text-align:center;background:#4bacc6;border-color:#4bacc6;float:right">Save</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%;text-align:center;background:#4bacc6;border-color:#4bacc6;float:right">Publish</button>
                     </div>

						  	 
						  <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 1"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">View</button>
                       </div>

                        
						  <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 2"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">View</button>
                       </div>

                        
						  <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 3"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">View</button>
                       </div>

                        
						  <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 4"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">View</button>
                       </div>

                       <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 5"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">View</button>
                       </div>

						  	</div>


						 <div class="col-md-6">

						  	<div class="card-header   bg-vd rounded-0 p-2" >
						  	<h5 class="text-center text-light m-0">Document Bank</h5>
						  </div>
						 
						  <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Save</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Save</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Save</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%;text-align:center;background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Save</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Save</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> --><br>
							</div>


								<div class="card-header   bg-vd rounded-0 p-2" >
						  	<h5 class="text-center text-light m-0">Document Recieved</h5>
						  </div>
						 
						  <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">View</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">View</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">View</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%;text-align:center;background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">View</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>

							 <div class="row row-eq-height card-body" style="display:inline;">
						  	 <input type="file" name="document" id="document" style="width:50%;">
						  	 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">View</button>
                      <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;">Send to</button>
							  <!-- </div> -->
							</div>




						  	</div>



		                  <div class="col-md-6">
						  	 <div class="card-header   bg-vd rounded-0 p-2" >
						  	<h5 class="text-center text-light m-0">Answer Questions</h5>
						  	 
						  </div> 
						  <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 1"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Respond</button>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Send to</button>
                       </div>
                       <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 2"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Respond</button>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Send to</button>
                       </div>
                       <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 3"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Respond</button>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Send to</button>
                       </div>
                       <div class="row row-eq-height card-body">
	<textarea class="form-control" type="text" name="question" id="question" placeholder="Question 4"></textarea>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Respond</button>
				 <button class="form-control btn btn-primary"  type="button" style="width:20%; background:#4bacc6;border-color:#4bacc6;float:right">Send to</button>
                       </div>
						  	</div>
 
		 
</div></div>
					</div>
				  </div>
				 </div>
				 

				 