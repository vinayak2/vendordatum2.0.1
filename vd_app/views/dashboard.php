<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
				<ul class="nav nav-pills nav-justified mt-3" role="tablist">
				  <li class="nav-item">
				    <a class="nav-link rounded-0 mr-1 active" data-toggle="pill" href="#catalog">Catalog</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link rounded-0 mr-1" data-toggle="pill" href="#accomplishments">Accomplishments</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link disabled rounded-0 mr-1" href="#" data-toggle="tooltip" title="Upcoming feature!">Knowledge Bank <i class="fas fa-exclamation-circle"></i></a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link disabled rounded-0 mr-1" href="#" data-toggle="tooltip" title="Upcoming feature!">Manage Publishes <i class="fas fa-exclamation-circle"></i></a>
				  </li>
				  <li class="nav-item">
				    <form action="<?php echo base_url('post'); ?>" method="get">
				    <div class="input-group">
					  <input type="text" class="form-control" name="search" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
					  <div class="input-group-append">
					    <button class="btn btn-outline-info" type="submit"><i class="fas fa-search"></i></button>
					  </div>
					</div>
					</form>
				  </li>
				</ul>
				<div class="tab-content">
				  <div class="tab-pane container active" id="catalog">
					<div class="row border-right">
				<?php if(isset($announcements) && !empty($announcements)): ?>
					  <div class="col-md-4">
						 <div class="row">
					<?php $i = 0; foreach($announcements as $item): $i++; ?>
						  <div class="col-sm-4 p-0 dropdown">
							<div class="border border-right-0" data-toggle="dropdown">
						  	<?php if($item->post_id): ?>
								<img src="<?php echo base_url('assets/uploads/post/resized/'); if($item->image): echo $item->image; else: echo 'placeholder.png'; endif; ?>" class="img-fluid" alt="Product">
								<?php if($item->publish): ?>
								<span class="badge badge-info rounded-0 vd-item-badge">Published</span>
								<?php else: ?>
								<span class="badge badge-secondary rounded-0 vd-item-badge">Pending</span>
								<?php endif; ?>
							<?php else: ?>
								<div class="vd-stack">
								  <div class="text">Add Item</div>
								</div>
							<?php endif; ?>								
							</div>
							<div class="dropdown-menu dropdown-menu-right p-0 border-0 rounded-0">
								<div class="product-form p-1 announcement">
									<?php echo form_open_multipart(); ?>
							            <?php echo form_input(array(
							            	'name' => 'title',
							            	'class' => 'form-control rounded-0',
							            	'placeholder' => 'Item Name',
							            	'value' => $item->title,
							            	'required' => 'required',
							            )); ?>
							            <?php echo form_input(array(
							            	'name' => 'description',
							            	'class' => 'form-control rounded-0',
							            	'placeholder' => 'Item Description',
							            	'value' => $item->description,
							            	'required' => 'required',
							            )); ?>
							            <?php echo form_input(array(
							            	'class' => 'form-control rounded-0 autocomplete-category',
							            	'placeholder' => 'Category',
							            	'value' => $item->category,
							            	'autocomplete' => 'off',
							            )); ?>
										<div class="form-control rounded-0 auto-suggesstion p-0"></div>
							            <?php echo form_hidden('category', $item->category); ?>
										<div class="custom-file">
										  <input type="file" class="custom-file-input" id="post_image" name="post_image">
										  <label class="custom-file-label rounded-0" for="post_image">Choose Image</label>
										</div>
										<div class="custom-file">
										  <input type="file" class="custom-file-input" id="post_attachment_<?php echo $i; ?>" name="post_attachment">
										  <label class="custom-file-label rounded-0" for="post_attachment_<?php echo $i; ?>">Attachment</label>
										</div>
										<div class="btn-group d-flex">
								            <button type="submit" name="submit" class="btn btn-info text-light rounded-0 w-100">Save</button>
											<?php if($item->publish): ?>
								            <button type="submit" name="unpublish" class="btn btn-secondary text-light rounded-0 w-100">Unpublish</button>
											<?php else: ?>
								            <button type="submit" name="publish" class="btn btn-info text-light rounded-0 w-100">Publish</button>
											<?php endif; ?>
											<?php if($item->post_id): ?>
								            <button type="submit" name="remove" class="btn btn-danger text-light rounded-0 w-100" onclick="return confirm('Are you sure you want to permanently remove this record?')">Remove</button>
							            	<?php echo form_hidden('post_id', $item->post_id); ?>
											<?php endif; ?>
							            </div>
							            <?php echo form_hidden('post_type', 'announcements'); ?>
						        	<?php echo form_close(); ?>
								</div>
							</div>
						  </div>
					<?php if(!($i%3)): ?>
						</div> 
					  </div>
					  <div class="col-md-4">
						 <div class="row">
					<?php endif; ?>
					<?php endforeach; ?>
						</div> 
					  </div>
				<?php endif; ?>
					</div>
				  </div>

				  <div class="tab-pane container fade" id="accomplishments">
					<div class="row border-right">
				<?php if(isset($accomplishments) && !empty($accomplishments)): ?>
					  <div class="col-md-4">
						 <div class="row">
					<?php $i = 0; foreach($accomplishments as $item): $i++; ?>
						  <div class="col-sm-4 p-0 dropdown">
							<div class="border border-right-0" data-toggle="dropdown">
						  	<?php if($item->post_id): ?>
								<img src="<?php echo base_url('assets/uploads/post/resized/'); if($item->image): echo $item->image; else: echo 'placeholder.png'; endif; ?>" class="img-fluid" alt="Product">
								<?php if($item->publish): ?>
								<span class="badge badge-info rounded-0 vd-item-badge">Published</span>
								<?php else: ?>
								<span class="badge badge-secondary rounded-0 vd-item-badge">Pending</span>
								<?php endif; ?>
							<?php else: ?>
								<div class="vd-stack">
								  <div class="text">Add Project</div>
								</div>
							<?php endif; ?>								
							</div>
							<div class="dropdown-menu dropdown-menu-right p-0 border-0 rounded-0">
								<div class="product-form p-1">
									<?php echo form_open_multipart(); ?>
							            <?php echo form_input(array(
							            	'name' => 'title',
							            	'class' => 'form-control rounded-0',
							            	'placeholder' => 'Project Name',
							            	'value' => $item->title,
							            	'required' => 'required',
							            )); ?>
							            <?php echo form_input(array(
							            	'name' => 'description',
							            	'class' => 'form-control rounded-0',
							            	'placeholder' => 'Project Scope',
							            	'value' => $item->description,
							            	'required' => 'required',
							            )); ?>
							            <?php echo form_input(array(
							            	'name' => 'category',
							            	'class' => 'form-control rounded-0',
							            	'placeholder' => 'Client / Customer',
							            	'value' => $item->category,
							            	'required' => 'required',
							            )); ?>
										<div class="custom-file">
										  <input type="file" class="custom-file-input" id="post_image" name="post_image">
										  <label class="custom-file-label rounded-0" for="post_image">Choose Image</label>
										</div>
										<div class="custom-file">
										  <input type="file" class="custom-file-input" id="post_attachment_<?php echo $i; ?>" name="post_attachment">
										  <label class="custom-file-label rounded-0" for="post_attachment_<?php echo $i; ?>">Attachment</label>
										</div>
										<div class="btn-group d-flex">
								            <button type="submit" name="submit" class="btn btn-info text-light rounded-0 w-100">Save</button>
											<?php if($item->publish): ?>
								            <button type="submit" name="unpublish" class="btn btn-secondary text-light rounded-0 w-100">Unpublish</button>
											<?php else: ?>
								            <button type="submit" name="publish" class="btn btn-info text-light rounded-0 w-100">Publish</button>
											<?php endif; ?>
											<?php if($item->post_id): ?>
								            <button type="submit" name="remove" class="btn btn-danger text-light rounded-0 w-100" onclick="return confirm('Are you sure you want to permanently remove this record?')">Remove</button>
							            	<?php echo form_hidden('post_id', $item->post_id); ?>
											<?php endif; ?>
							            </div>
							            <?php echo form_hidden('post_type', 'accomplishments'); ?>
						        	<?php echo form_close(); ?>
								</div>
							</div>
						  </div>
					<?php if(!($i%3)): ?>
						</div> 
					  </div>
					  <div class="col-md-4">
						 <div class="row">
					<?php endif; ?>
					<?php endforeach; ?>
						</div> 
					  </div>
				<?php endif; ?>
					</div>
				  </div>
				  <div class="tab-pane container fade" id="menu2">...</div>
				  <div class="tab-pane container fade" id="menu3">...</div>
				</div>

				<?php
				if(isset($latest_posts) && !empty($latest_posts)): ?>
				<div class="row mt-3">
				  <?php $i = 0; foreach($latest_posts as $item): $i++;  ?>
				  <div class="col-md-6">
				  	<div class="border px-2">
						<div class="card border-0 rounded-0 mt-2">
						  <div class="card-header bg-vd rounded-0 p-2">
						  	<h5 class="text-center text-light m-0"><?php echo $item->title; ?></h5>
						  </div>
						  <div class="row row-eq-height card-body">
							  <div class="col-md-8 p-1">
							  	<div class="description border p-2">
							  		<p><?php echo $item->description; ?></p>
							  	</div>
							  	<div class="d-flex flex-wrap">
								  <a<?php if($item->attachment): ?> href="<?php echo base_url('assets/uploads/post/attachment/').$item->attachment; ?>" target="_blank" class="btn btn-light border rounded-0 flex-fill"<?php else: ?> class="btn btn-light border rounded-0 flex-fill disabled"<?php endif; ?>>View attachment</a>
								  <a href="<?php echo base_url('post/').$item->post_id; ?>" class="btn btn-light border rounded-0 flex-fill">View details</a>
								</div>
							  	<div class="d-flex flex-wrap">
							  		<a href="profile/<?php echo $item->added_by; ?>" class="btn btn-link border rounded-0 flex-fill" role="button"><?php echo $item->getCompanyName(); ?></a>
							  		<a href="profile/<?php echo $item->added_by; ?>" class="btn btn-link border rounded-0 flex-fill" role="button"><?php echo $item->getAuthorName(); ?></a>
							  	</div>
							  </div>
							  <div class="col-md-4 p-1">
						<?php echo $this->utilities->prep_display_image($item->image, 'post'); ?>
							  </div>
						  </div>
						</div>
					</div>
				  </div>
				<?php if(!($i%2)): ?>
				</div>
				<div class="row mt-3">
				<?php endif; ?>
				  <?php endforeach; ?>
				</div>
				<?php endif; ?>

				<?php echo $pagination; ?>  