<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>        				 
        <div class="row mt-3">
          <div class="col-md-12">
            <div class="border px-2">
              <div class="card border-0 rounded-0 mt-2">
                <div class="row">
                  <div class="col-md-12 p-2">
                    <form action="<?php echo base_url('post'); ?>" method="get" class="w-100">
                      <div class="row row-eq-height card-body text-center">
                        <div class="col-md-12 ">
                          <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2" value="<?php echo $request['search']; ?>">
                            <div class="input-group-append">
                              <button class="btn btn-outline-info" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- <div class="row row-eq-height card-body text-center"> -->
                        <!-- <div class="col-md-4 ">
                          <div class="form-group">
                            <p class="border mt-3">Product / Service</p> -->
                            <!-- <input type="text" class="form-control" name="title" aria-label="Search" aria-describedby="basic-addon2" value="<?php //echo $request['title']; ?>"> -->
                            <!-- <select id="inputState" class="form-control">
                              <option selected>Select</option>
                              <option>Industry 1</option>
                              <option>Industry 2</option>
                              <option>Industry 3</option>
                            </select> -->
                          <!-- </div>
                        </div> -->
                        <!-- <div class="col-md-4 ">
                          <p class="border mt-3">Company Name</p>
                          <div class="form-group"> -->
                            <!-- <input type="text" class="form-control" name="company" aria-label="Search" aria-describedby="basic-addon2" value="<?php //echo $request['company']; ?>"> -->
                            <!-- <select id="inputState" class="form-control ">
                              <option selected>Select</option>
                              <option>High to Low</option>
                              <option>Low to High</option>
                            </select> -->
                          <!-- </div>
                        </div> -->
                        <!-- <div class="col-md-4 ">
                          <p class="border mt-3">Location</p>
                          <div class="form-group"> -->
                            <!-- <input type="text" class="form-control" name="location" aria-label="Search" aria-describedby="basic-addon2" value="<?php //echo $request['location']; ?>"> -->
                            <!-- <select id="inputState" class="form-control">
                              <option selected>Select</option>
                              <option>Older Post</option>
                              <option>Most Recent</option>
                            </select> -->
                          <!-- </div>
                        </div> -->
                      <!-- </div> -->
                    </form>	 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php
         if(isset($posts) && !empty($posts)): ?>
        <div class="row mt-3">
          <?php $i = 0; foreach($posts as $item): $i++; ?>
          <div class="col-md-6">
            <div class="border px-2">
            <div class="card border-0 rounded-0 mt-2">
              <div class="card-header bg-vd rounded-0 p-2">
                <h5 class="text-center text-light m-0"><?php echo $item->title; ?></h5>
              </div>
              <div class="row row-eq-height card-body">
                <div class="col-md-8 p-1">
                  <div class="description border p-2">
                    <p><?php echo $item->description; ?></p>
                  </div>
                  <div class="d-flex">
                  <button type="button" class="btn btn-light border rounded-0 w-100">View attachment</button>
                  <a href="<?php echo base_url('post/').$item->post_id; ?>" class="btn btn-light border rounded-0 w-100">View details</a>
                </div>
                  <div class="d-flex">
                    <a href="profile/<?php echo $item->added_by; ?>" class="btn btn-link border rounded-0 w-100" role="button"><?php echo $item->getCompanyName(); ?></a>
                    <a href="profile/<?php echo $item->added_by; ?>" class="btn btn-link border rounded-0 w-100" role="button"><?php echo $item->getAuthorName(); ?></a>
                  </div>
                </div>
                <div class="col-md-4 p-1">
            <?php echo $this->utilities->prep_display_image($item->image, 'post'); ?>
                </div>
              </div>
            </div>
          </div>
          </div>
        <?php if(!($i%2)): ?>
        </div>
        <div class="row mt-3">
        <?php endif; ?>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>

        <?php echo $pagination; ?>  