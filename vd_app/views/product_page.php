<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>	 
         <div class="row mt-3">
			<div class="col-md-12">
				<div class="border px-2">
					<div class="card border-0 rounded-0 mt-2">
						<div class="card-header bg-vd rounded-0 p-2">
							<h5 class="text-center text-light m-0">Item View (Product / Service)</h5>
						</div>
						<div class="row row-eq-height card-body">
							<div class="col-md-9">								
								<h2><?php echo $post->title; ?></h2>
								<div class="float-left w-50 mr-2 mb-2">
						    		<?php echo $post_image; ?>
								</div>
						    	<?php echo $post->description; ?>
						    	<?php if($post->attachment): ?>
								<div class="border-top pt-2">
									<a href="<?php echo base_url('assets/uploads/post/attachment/').$post->attachment; ?>" target="_blank" class="btn btn-light border rounded-0"><i class="fa fa-download"></i> Download Attachment</a>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-md-3 border-left">
								<h5 class="text-center"><?php echo $profile_username; ?></h5>
								<?php echo $profile_image; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>