<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* This class extends the CI_Model and include the basic CRUD functionalities.
 * All individual models for each database tables will extend MY_Model.
 * Authors: Lijo George, 
 * Owned by: Cuion Technologies Pvt. Ltd.
 * Owner URL: http://www.cuion.in/
 */

class MY_Model extends CI_Model {
	
	/* Database table name */
    const DB_TABLE = 'abstract';
	
	/* Database table primary key */
    const DB_TABLE_PK = 'abstract';
	
	/* Create record.
     */
    private function insert() {
        $this->db->insert($this::DB_TABLE, $this);
        $this->{$this::DB_TABLE_PK} = $this->db->insert_id();
    }
    
    /* Update record.
     */
    private function update() {
        $this->db->update($this::DB_TABLE, $this, array(
           $this::DB_TABLE_PK => $this->{$this::DB_TABLE_PK}, 
        ));
    }
    
    /* Populate from an array or standard class.
     * @param mixed $row
     */
    public function populate($row) {
		if(isset($row) && !empty($row)){
			foreach ($row as $key => $value) {
				$this->$key = $value;
			}
		}
    }
    
    /* Load from the database.
     * @param int $id
     */
    public function load($id) {
        $query = $this->db->get_where($this::DB_TABLE, array(
            $this::DB_TABLE_PK => $id,
        ));
        $this->populate($query->row());
    }
    
    /* Delete the current record.
     */
    public function delete() {
        $this->db->delete($this::DB_TABLE, array(
           $this::DB_TABLE_PK => $this->{$this::DB_TABLE_PK}, 
        ));
        unset($this->{$this::DB_TABLE_PK});
    }
    
    /* Save the record.
     */
    public function save() {
        if (isset($this->{$this::DB_TABLE_PK})) {
            $this->update();
        }
        else {
            $this->insert();
        }
    }
    
    /* Get an array of Models with an optional limit, offset.
     * 
     * @param int $limit Optional.
     * @param int $offset Optional; if set, requires $limit.
     * @param array $conditions Optional.
     * @param string $order_by Optional. Default column 'added_on'.
     * @param string $order Optional. Default option 'DESC'.
     * @return array Models populated by database, keyed by PK.
     */
    public function get($limit = NULL, $offset = NULL, $conditions = NULL){
        $query = $this->db->get_where($this::DB_TABLE, $conditions, $limit, $offset);
        $ret_val = array();
        $class = get_class($this);
        foreach ($query->result() as $row) {
            $model = new $class;
            $model->populate($row);
            $ret_val[$row->{$this::DB_TABLE_PK}] = $model;
        }
        return $ret_val;
    }
    
    /* Get an array of Models for dropdown list.
     * 
     * @param string $column Optional.
     * @param array $conditions Optional.
     * @param string $order_by Optional. Default column 'added_on'.
     * @param string $order Optional. Default option 'DESC'.
     * @return array Models populated by database, keyed by PK.
     */
	
	public function getList($column = NULL, $conditions = NULL){
		if($column){
			$this->db->select($this::DB_TABLE_PK);
			$this->db->select($column.' as value');
			$query = $this->db->get_where($this::DB_TABLE, $conditions);
			$ret_val = array();
			foreach ($query->result() as $row){
				$ret_val[$row->{$this::DB_TABLE_PK}] = $row->value;
			}
			return $ret_val;
		}
	}
    
    /* Get number of rows.
     * 
     * @param int $limit Optional.
     * @param int $offset Optional; if set, requires $limit.
     * @param array $conditions Optional.
     * @param string $order_by Optional. Default column 'added_on'.
     * @param string $order Optional. Default option 'DESC'.
     * @return array Models populated by database, keyed by PK.
     */    
    
    public function getCount($limit = NULL, $offset = NULL, $conditions = NULL){
        $this->db->select('COUNT('.$this::DB_TABLE_PK.') as value');
        return $this->db->get_where($this::DB_TABLE, $conditions, $limit, $offset)->row()->value;
    }
	
	public function slug(){
		if($this->title) return strtolower(str_replace(' ', '_', $this->title));
	}
}