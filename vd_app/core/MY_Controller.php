<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* This class extends the CI_Controller and include the general functionalities.
 * All individual Controllers will extend MY_Controller.
 * Authors: Lijo George, 
 * Owned by: Cuion Technologies Pvt. Ltd.
 * Owner URL: http://www.cuion.in/
 */

class MY_Controller extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->data = array();
		date_default_timezone_set('Asia/Kolkata');
    }
	
	/* Function to render complete HTML page
	 * combining partial components like header, footer etc.
	 * @param $view string Represents the view to be loaded. Default 'home'.
	 * @param $data array Data for page body (optional).
	 * @param $optionals array Lists optional parts to be included in the page.
	 * 		ex. 'adminpanel' gives AdminPanel View, 'search' include optional Search Block
	 */
	protected function render($view = 'index', $data = null, $optionals = array())
    {		
		// load header with $data
		$this->load->view('inc/header', $data);
		
		// load optional home slider
		if(in_array("user_panel", $optionals)){
			$this->load->view('inc/user_panel');
		}
		
		// load view
		$this->load->view($view);
		
		// load footer
		$this->load->view('inc/footer'); 	
    }

	/*
	* This function will automatically crop image to required size with best possible output.
	*
	* $terms = array(
	*	'source' => 'path/filename.jpg',				// Source file
	*	'result' => 'new_path/new_filename.jpg',		// Result file
	*	'source_width' => 'pixels',						// Width of the Source file in pixels
	*	'source_height' => 'pixels',					// Height of the Source file in pixels
	*	'result_width' => 'pixels',						// Width of the Result file in pixels
	*	'result_height' => 'pixels'						// Height of the Result file in pixels
	* );
	*/
	public function BestCropImage($terms)
	{
		if($terms['result_width'] > (($terms['source_width'] * $terms['result_height']) / $terms['source_height'])){
			$resize_width = $terms['result_width'];
			$resize_height = $terms['source_height'];
		}else{
			$resize_width = $terms['source_width'];
			$resize_height = $terms['result_height'];
		}
		$config = array(
			'image_library' => 'gd2',
			'source_image' => $terms['source'],
			'new_image' => $terms['result'],
			'width' => $resize_width,
			'height' => $resize_height,
		);
		$this->load->library('image_lib', $config, 'img_resize');
		/*$this->img_resize->resize();*/
		if ( ! $this->img_resize->resize())
		{
		        echo $this->img_resize->display_errors();
		}
		$config = array(
			'image_library' => 'gd2',
			'source_image' => $terms['result'],
			'maintain_ratio' => FALSE,
			'width' => $terms['result_width'],
			'height' => $terms['result_height'],
		);
		$this->load->library('image_lib', $config, 'img_crop');
		/*$result = $this->img_crop->crop();*/
		if ( ! $this->img_crop->crop())
		{
		        echo $this->img_crop->display_errors();
		}
	}
}