<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* This library includes custom functionaliteis.
 * Authors: Lijo George, 
 * Owned by: Cuion Technologies Pvt. Ltd.
 * Owner URL: http://www.cuion.in/
 */
class Utilities
{	
	public function prep_display_image($image = null, $image_dir = '', $extras = array())
	{
		if($image):
			$ret_val = '<a href="'. base_url('assets/uploads/') . $image_dir . '/' . $image . '" data-fancybox><img src="'. base_url('assets/uploads/') . $image_dir . '/resized/' . $image . '"';
		else:
			$ret_val = '<img src="'. base_url('assets/uploads/') . $image_dir . '/resized/placeholder.png"';
		endif;
		if(isset($extras) && !empty($extras)):
			foreach($extras as $property => $value):
				$ret_val .= ' '.$property.'="'.$value.'"';
			endforeach;
		else:
			$ret_val .= ' class="img-thumbnail img-fluid rounded-0 image_preview" alt=""';
		endif;
		if($image):
			$ret_val .= ' /></a>';
		else:
			$ret_val .= ' />';
		endif;
		return $ret_val;
	}
}