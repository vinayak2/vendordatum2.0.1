<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = 'auth/page_not_exist';
$route['translate_uri_dashes'] = FALSE;

/* Authentication */
$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';
$route['onboard/(:any)'] = 'auth/register/$1';
$route['main/(:any)'] = 'main/index/$1';
$route['remind_password'] = 'auth/forgot_password';
$route['change_password'] = 'auth/change_password';
$route['reset_password'] = 'auth/reset_password';
$route['reset_password/(:any)'] = 'auth/reset_password/$1';
$route['register_success'] = 'auth/register_success';
$route['profile/(:any)'] = 'main/profile/$1';
$route['profile'] = 'main/profile';
$route['settings'] = 'auth/edit_user';

$route['post/(:any)/(:any)'] = 'main/post/$1/$2';
$route['post/(:any)'] = 'main/post/$1';
$route['post'] = 'main/post';

/* AJAX */
$route['categories'] = 'main/ajax_autoload_category';

$route['account'] = 'main/account';
$route['publisher'] = 'main/publisher';
$route['knowledge_bank'] = 'main/knowledge_bank';
$route['question_asked'] = 'main/question_asked';
$route['answer_questions'] = 'main/answer_questions';

$route['test_email'] = 'main/test_email';