<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Pagination custom configurations for Bootstrap 4
 */

$config = array(
    'base_url' => base_url(),
    'full_tag_open' => '<ul class="pagination justify-content-center mt-3">',
    'full_tag_close' => '</ul>',
    'first_link' => '<i class="fas fa-angle-double-left"></i> First',
    'first_tag_open' => '<li class="page-item">',
    'first_tag_close' => '</li>',
    'last_link' => 'Last <i class="fas fa-angle-double-right"></i>',
    'last_tag_open' => '<li class="page-item">',
    'last_tag_close' => '</li>',
    'prev_link' => '<i class="fas fa-angle-left"></i> Previous',
    'prev_tag_open' => '<li class="page-item">',
    'prev_tag_close' => '</li>',
    'next_link' => 'Next <i class="fas fa-angle-right"></i>',
    'next_tag_open' => '<li class="page-item">',
    'next_tag_close' => '</li>',
    'cur_tag_open' => '<li class="page-item active"><a href="#" class="page-link" disabled>',
    'cur_tag_close' => '</a></li>',
    'num_tag_open' => '<li class="page-item">',
    'num_tag_close' => '</li>',
    'attributes' => array('class' => 'page-link'),
);