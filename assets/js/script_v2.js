/* Select2 */
$(function () {
    $(".select2").select2();
});

// ===== Autocomplete Category =====
$(document).ready(function() {
    $(".autocomplete-category").keyup(function(){
        var element = $(this);
        element.next().next().val('');
        $.ajax({            
            type: "POST",
            url: variable.base_url + "categories",
            data:'keyword=' + $(this).val(),
            beforeSend: function(){
                element.addClass("loading-gif");
            },
            success: function(data){
                if(data){
                    element.removeClass("text-danger");
                    element.next().html(data);
                    element.next().slideDown();
                }else{
                    element.addClass("text-danger");
                    element.next().slideUp();
                }
                element.removeClass("loading-gif");
            }
        });
    });

    $("div.auto-suggesstion").on("click", "li", function(){
        var item = $(this);
        item.parent().parent().prev().val(item.text().trim());
        item.parent().parent().next().val(item.text().trim());
        item.parent().parent().slideUp();
    });

    $('div.announcement form').submit(function(event){
        if(!$(this).children("input[name='category']").val()){
            event.preventDefault();
        }
    });
});